
#include "basics.h"

void			*mem_join(void const *p1, size_t len_1, void const *p2,
							size_t len_2) {
	uint32_t	total_len;
	void		*p3;

	if ((total_len = len_1 + len_2) == 0)
		return NULL;
	p3 = malloc(total_len);
	if (p3 == NULL )
		return NULL;
	if (len_1 > 0)
		mem_cpy(p3, p1, len_1);
	if (len_2 > 0)
		mem_cpy(p3 + len_1, p2, len_2);
	return p3;
}
