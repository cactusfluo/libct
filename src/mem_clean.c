
#include "basics.h"

void			mem_clean(void *s, size_t n) {
	uint64_t	*mem_64;
	uint8_t		*mem_8;
	size_t		div;
	uint8_t		mod;
	size_t		i;

	mem_64 = (uint64_t *)s;
	div = n / 8;
	mod = n % 8;
	i = 0;
	while (i++ < div) {
		*mem_64 = 0;
		mem_64++;
	}
	mem_8 = (uint8_t *)mem_64;
	i = 0;
	while (i < mod)
		mem_8[i++] = 0;
}
