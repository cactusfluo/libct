
#include "basics.h"

void		*mem_set(void *ptr, char c, size_t length) {
	char	*memory;
	size_t	i;

	memory = ptr;
	i = 0;
	while (i < length)
		memory[i++] = (unsigned char)c;
	return ptr;
}
